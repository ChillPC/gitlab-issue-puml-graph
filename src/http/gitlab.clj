(ns http.gitlab
  (:require [clj-http.client :as client]
            [clj-json.core :as json]
            [clojure.string :as str]))

(defn trim-base-url [base-url]
  (str/replace base-url #"/*$" ""))

(defn -get-auth [url auth-token]
  (let [query-params {"state" "all" "scope" "all"}
        options
        {:headers {"authorization" (str "Bearer " auth-token)}
         :query-params query-params}]
    (try
      (client/get
       url
       options)
      (catch Exception error
        (throw (as-> (ex-data error) $
                 (assoc $ :request {:url url :options options})
                 (ex-info (.getMessage error) $)))))))

(defn -get-issue-list-url
  ([base-url] (-get-issue-list-url base-url {}))
  ([base-url {group-id :group-id
              epic-id :epic-id}]
   (str
    (trim-base-url base-url)
    "/api/v4"
    (if group-id
      (str
       "/groups/" group-id
       (if epic-id
         (str "/epics/" epic-id)
         ""))
      "")
    "/issues")))

(defn fetch-issues
  ([url auth-token opts]
   (let [response
         (-> (-get-issue-list-url url opts)
             (-get-auth auth-token))]
     {:issues
      (-> (response :body)
          (json/parse-string))})))

(defn -get-link-list-url [base-url project-id issue-id]
  (str base-url
       "/api/v4/projects/" (str/replace project-id "/" "%2F") "/issues/" issue-id "/links"))

(defn fetch-links [base-url auth-token project-id issue-id]
  (try
    (let [response
          (-get-auth
           (-get-link-list-url base-url project-id issue-id)
           auth-token)]
      (-> response (get :body)  (json/parse-string)))
    (catch Exception e (println (.getMessage e)))))
