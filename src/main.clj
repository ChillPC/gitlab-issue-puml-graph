(ns main
  (:require
   [cli-matic.core :refer [run-cmd]]
   [clojure.spec.alpha :as s]
   [core.gitlab.issue]
   [core.gitlab.link]
   [core.issue :as issue]
   [http.gitlab]
   [clojure.tools.trace]
   [plantuml :as puml]))

(defn merge-link-accs
  [link-acc0 link-acc1]
  (reduce
   (fn [acc [issue-id issues-blocked1]]
     (->> (get acc issue-id #{})
          (conj issues-blocked1)
          (assoc acc issue-id)))
   link-acc0
   (seq link-acc1)))

(defn generate-gitlab
  [{group :group epic :epic url :url auth-token :auth-token include-nearest :include-nearest include-description :description}]
  (let [opts {:group-id group
              :epic-id epic}

        issues-by-id
        (-> (http.gitlab/fetch-issues url auth-token opts)
            (:issues)
            (core.gitlab.issue/api-maps->issue-by-id))

        issues-by-epic
        (issue/issues-by-epic (vals issues-by-id))

        ; For now, include-nearest always true
        {new-issues-by-id :issues-by-id new-issues-by-epic :issues-by-epic link-acc :link-acc}
        (reduce
         (fn [{issues-by-id :issues-by-id issues-by-epic :issues-by-epic link-acc :link-acc}
              issue]
           (let [{new-issues-by-id :new-issues-by-id new-link-acc :link-acc}
                 (->> (http.gitlab/fetch-links url auth-token (:project-id issue) (:iid issue))
                      (map core.gitlab.link/api-map->link)
                      (core.gitlab.link/issue-links-sorting issue issues-by-id))
                 new-acc
                 {:issues-by-id
                  (reduce #(assoc %1 (first %2) (rest %2))
                          issues-by-id new-issues-by-id)
                  :issues-by-epic
                  (->> (vals new-issues-by-id)
                       (issue/issues-by-epic issues-by-epic))
                  :link-acc (merge-link-accs link-acc new-link-acc)}]
             new-acc))
         {:issues-by-id issues-by-id :issues-by-epic issues-by-epic :link-acc {}}
         (vals issues-by-id))]

    (plantuml/enclose-plantuml
     (str
      (plantuml/issues-by-epic->plantuml new-issues-by-epic include-description)
      (plantuml/links->plantuml link-acc)))))

(s/def :config/state #{:opened :closed :all})
(s/def :config/pack-by #{:none :epic :project})

(def CONFIGURATION
  {:app {:command "gitlab-issue-puml-graph"
         :description "Generate issue graph of gitlab issue"
         :version "0.0.0"}

   :global-opts [{:option "url" :short "u"
                  :as "Url of the gitlab instance"
                  :type :string
                  :default  "https://gitlab.com"}
                 {:option "auth-token" :short "a"
                  :as "Gitlab auth token"
                  :type :string
                  :env "GITLAB_AUTH_TOKEN"}]

   :commands [{:command "gitlab"
               :short "gl"
               :description "Generate on stdout"
               :runs generate-gitlab

               :opts
               [;; Display in issue
                {:option "description" :short "d"
                 :type :with-flag
                 :default false}
                {:option "pack" :short "p"
                 :type :keyword
                 :default :epic
                 :spec :config/pack-by}

                ;; Filter issues
                {:option "include-nearest" :short "i"
                 :type :with-flag
                 :default false}
                {:option "group" :short "g"
                 :type :string
                 :default nil}
                {:option "epic" :short "e"
                 :type :string
                 :default nil}
                {:option "state" :short "s"
                 :as "State of the ticket (opened, closed or all)"
                 :type :keyword
                 :default :opened
                 :spec :config/state}]}]})

(defn -main [& args]
  (run-cmd args CONFIGURATION))
