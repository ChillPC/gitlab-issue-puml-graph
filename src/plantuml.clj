(ns plantuml
  (:require
   [clj-json.core :as json]
   [clojure.string :as str]))

(defn enclose-plantuml [uml]
  (str "@startuml\n" uml "\n@enduml"))

(defn -clj->json-k-v
  ([k v] (-clj->json-k-v k v false))
  ([k v is-raw-string]
   (str "\"" (name k) "\":"
        (if is-raw-string v (str "\"" v "\"")))))

(defn issue->puml-object [issue]
  (->
   (dissoc issue
           :forged-id
           :title)))

(defn links->plantuml [link-acc]
  (reduce
   (fn [acc [key val]]
     (str
      acc
      "\n\n"
      (str/join (str "\n" key " -> ") val)))
   ""
   link-acc))

(defn sanitize [txt]
  (if txt
    (-> txt
        (str/replace "\"" "\\\""))
    txt))

;; Hideous (we want field in order) but sufficient
(defn issue->json-plantuml-str [issue include-description]
  (str
   "{\n"
   (str/join ",\n"
             (-> [(-clj->json-k-v "weight" (let [w (-> (:weight issue) (sanitize))] (if w w "null")) true)
                  (-clj->json-k-v "properties" (json/generate-string (:properties issue)) true)
                  (-clj->json-k-v "labels" (json/generate-string (:labels issue)) true)
                  (-clj->json-k-v "author" (-> (:author issue) (sanitize)))
                  (-clj->json-k-v "assignees" (json/generate-string (:assignees issue)) true)
                  (-clj->json-k-v "ref" (-> (:ref issue) (sanitize)))]))
   (if include-description
     (-clj->json-k-v "description" (-> (:description issue) (sanitize) (str/split-lines) (json/generate-string)) true)
     "")
   "\n}"))

(defn issue->plantuml [issue include-description]
  (str "\njson \"" (:title issue) "\"" " as " (:forged-id issue) " "
       (issue->json-plantuml-str issue include-description)))

(defn -issues-in-epic->plantuml [epic issues include-description]
  (let [issues-str (str/join (map #(issue->plantuml % include-description) issues))]
    (if epic
      (str
       "\npackage \"" (-> (:name epic) (sanitize)) "\" as epic_" (:id epic) " {\n"
       issues-str
       "\n}")
      issues-str)))

(defn issues-by-epic->plantuml [issues-by-epic-id include-description]
  (str/join
   "\n"
   (map #(-issues-in-epic->plantuml (:epic %) (:issues %) include-description)
        (vals issues-by-epic-id))))
