(ns core.gitlab.link
  (:require
   [core.gitlab.issue :as issue]))

(defn api-map->link
  "Take one of the issue linked and transform it into a well defined link"
  [api-map]
  {:issue (issue/api-map->issue api-map)
   :link-type (case (get api-map "link_type")
                "blocks" :blocks
                "is_blocked_by" :blocked-by
                nil)})

(defn api-map->links-by-id [api-maps]
  (reduce
   (fn [acc map]
     (let [link (api-map->link map)]
       (assoc acc (-> (:issue link) (:id)) link)))
   {}
   api-maps))

(defn issue-links-sorting [issue issues-by-id issue-links]
  (reduce
   (fn [{link-acc :link-acc new-issues-by-id :new-issues-by-id}
        {linked-issue :issue link-type :link-type}]
     (let [added-new-issues-by-id
           (if (or (find new-issues-by-id (:id linked-issue))
                   (find issues-by-id (:id linked-issue)))
             new-issues-by-id
             (assoc new-issues-by-id (:id linked-issue) linked-issue))

           [blocked-id blocker-id]
           (if (= link-type "blocks")
             [(:id issue) (:id linked-issue)]
             [(:id linked-issue) (:id issue)])

           already-blocked-ids (get link-acc blocker-id #{})
           added-link-acc (assoc link-acc blocker-id (conj already-blocked-ids blocked-id))]
       {:link-acc added-link-acc :new-issues-by-id added-new-issues-by-id}))
   {:link-acc {} :new-issues-by-id {}}
   issue-links))
