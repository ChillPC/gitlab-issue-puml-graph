(ns core.gitlab.issue
  (:require
   [clojure.string :as str]))

(defn -issue-forged-id [issue-id]
  ; (str "proj" project-id  "_issue" issue-id)
  (str "issue_" issue-id))

(defn -raw-labels->labels-props [labels]
  (->> labels
       (map #(str/split % #"::" 2))
       (reduce
        #(let [[k v] %2]
           (if (nil? v)
             (assoc %1 nil (conj (get %1 nil #{}) k))
             (conj %1 [k v])))
        {})))

(defn api-map->issue
  "From the map of the json of the api to a well defined issue"
  [api-map]
  (let [in-map #(get api-map %)
        label-props (-raw-labels->labels-props (get api-map "labels"))
        ; forged-id (-issue-forged-id (in-map "project_id") (in-map "iid"))
        forged-id (-issue-forged-id (in-map "id"))]

    {:forged-id forged-id

     :id (in-map "id")
     :iid (in-map "iid")
     :project-id (in-map "project_id")
     :ref (-> api-map
              (get "references")
              (get "full"))

     :title (in-map "title")
     :opened (= "opened" (in-map "state"))

     :weight (in-map "weight")

     :assignees (map #(get % "name") (get api-map "assignees"))
     :author (-> (in-map "author") (get "name"))

     :epic
     (if (in-map "epic_iid")
       {:id (-> (in-map "epic") (get "id"))
        :iid (in-map "epic_iid")
        :name (-> (in-map "epic") (get "title"))}
       nil)

     :description (in-map "description")
     :labels (get label-props nil)
     :properties (dissoc label-props nil)}))

(defn api-maps->issue-by-id
  [api-maps]
  (reduce
   (fn [acc api-map]
     (let [issue (api-map->issue api-map)]
       ; (println api-maps)
       (assoc acc (:id issue) issue)))
   {}
   api-maps))
