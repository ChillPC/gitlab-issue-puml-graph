(ns core.issue)

(defn issues-by-epic
  ([existing new-issues]
   (reduce
     #(let [epic-id (get-in %2 [:epic :id])
             epic-content
             (or (get %1 epic-id)
                 {:epic (:epic %2)
                  :issues '()})
             issues (conj (:issues epic-content) %2)
             new-epic-content (assoc epic-content :issues issues)]
         (assoc
           %1
           epic-id
           new-epic-content))
     existing new-issues))
  ([issues]
   (issues-by-epic {} issues)))

(defn issues-by-id [issues]
  (reduce #(assoc %1 (:id %2) %2) {} issues))
