(ns plantuml-test
  (:require
   [clj-json.core :as json]
   [clojure.test :refer [deftest is testing]]
   [core.gitlab.issue :as gl-issue]
   [gitlab.issue-fixture :as issue-fixture]
   [plantuml :as puml]
   [core.issue :as issue]))


(deftest clj->json-k-v
  (testing "Keyword key"
    (testing "Transform string"
      (is (= (puml/-clj->json-k-v :key "value" false)
             "\"key\":\"value\"")))

    (testing "Raw string"
      (is (let [value "{\"key2\": \"final-value\"}"]
            (= (puml/-clj->json-k-v :key value true)
               (str "\"key\":" value))))))

  (testing "String key"
    (testing "Transform string"
      (is (= (puml/-clj->json-k-v "key" "value" false)
             "\"key\":\"value\"")))

    (testing "Raw string"
      (is (let [value "{\"key2\": \"final-value\"}"]
            (= (puml/-clj->json-k-v :key value true)
               (str "\"key\":" value)))))))

; (comment
;  (print
;    (puml/elems->document
;      [(-> issue-fixture/map-fixture
;         (issue/api-map->issue)
;         (puml/issue->plantuml))])))


(comment
  (->> issue-fixture/map-fixture
      (map gl-issue/api-map->issue)
      (issue/issues-by-epic)
      (puml/issues-by-epic->plantuml)
      (println)))
