(ns http.gitlab-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [http.gitlab :as gl]))

(def base-url "https://gitlab.com")

(def api-v4-url (str base-url "/api/v4"))

(deftest get-issue-list-url
  (testing "Simple"
    (is (= (gl/get-issue-list-url
             base-url)
           (str api-v4-url "/issues"))))
  (testing "Triming slashes"
    (is (= (gl/get-issue-list-url (str base-url "/////"))
           (str api-v4-url "/issues"))))
  (testing "Project"
    (is (= (gl/get-issue-list-url (str base-url "/////") {:group-id "1111"})
           (str api-v4-url "/groups/1111/issues"))))
  (testing "Epic"
    (is (= (gl/get-issue-list-url (str base-url "/////") {:group-id "1111" :epic-id "1111"})
           (str api-v4-url "/groups/1111/epics/1111/issues")))))
