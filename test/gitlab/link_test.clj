(ns gitlab.link-test
  (:require [clojure.test :refer [are deftest testing is]]
            [gitlab.link-fixture :as fixt]
            [core.gitlab.link :as core-link]
            [core.gitlab.issue :as core-issue]))

(deftest api-map->link
  (testing "1001 blocks 1002"
    (let [links (get fixt/core-links-by-issue-id 1001)
          link (first links)]
      (is (= (count links) 1))
      (is (= (:link-type link)
             :blocks))
      (is (= (-> (:issue link)
                 (:id 1002))))))

  (testing "2001 blocks 1002"
    (let [links (get fixt/core-links-by-issue-id 2001)
          link (first links)]
      (is (= (count links) 1))
      (is (= (:link-type link)
             :blocks))
      (is (= (-> (:issue link)
                 (:id 1002))))))

  (testing "1002 blocked by 1002 and 2001"
    (let [links (get fixt/core-links-by-issue-id 1002)
          link1 (first links)
          link2 (-> (rest links) (first))]
      (is (= (count links) 2))
      (is (= (-> (:issue link1)
                 (:id 1001))))
      (is (= (:link-type link1)
             :blocked-by))
      (is (= (-> (:issue link2)
                 (:id 2001))))
      (is (= (:link-type link2)
             :blocked-by)))))

(deftest issue-links-sorting
  (let [issue-1002 (core-issue/api-map->issue (get fixt/issue-by-general-id 1002))
        links-of-1002 (map core-link/api-map->link (-> (get fixt/links-by-issue-id 1002)))]
    (testing "No issues-by-id"
      (let [{link-acc :link-acc new-issues-by-id :new-issues-by-id}
            (core-link/issue-links-sorting
             issue-1002
             links-of-1002
             {})]
        (is (= (count new-issues-by-id) 2))
        (is (= link-acc {1002 #{1001 2001}}))))
    (testing "Already 2001 in issues-by-id"
      (let [{link-acc :link-acc new-issues-by-id :new-issues-by-id}
            (core-link/issue-links-sorting
             issue-1002
             links-of-1002
             {2001 (-> (get fixt/issue-by-general-id 2001)
                       (core-issue/api-map->issue))})]
        (is (= (count new-issues-by-id) 1))
        (is (find new-issues-by-id 1001))
        (is (not (find new-issues-by-id 2001)))
        (is (= link-acc {1002 #{1001 2001}}))))))
