(ns gitlab.link-fixture
  (:require [clj-json.core :as json]
            [core.gitlab.link :as link]))

; (def links-str-fixture
;   (slurp "test/gitlab/link_fixture.json"))

; (def links-fixture
;   (json/parse-string links-str-fixture))

(def issue-by-general-id
  {1001
   {"web_url" "https://gitlab.com/my-group/project1/-/issues/1",
    "due_date" nil,
    "_links"
    {"self" "https://gitlab.com/api/v4/projects/1/issues/1",
     "notes" "https://gitlab.com/api/v4/projects/1/issues/1/notes",
     "award_emoji"
     "https://gitlab.com/api/v4/projects/1/issues/1/award_emoji",
     "project" "https://gitlab.com/api/v4/projects/1",
     "closed_as_duplicate_of" nil},
    "references"
    {"short" "#1", "relative" "#1", "full" "my-group/project1#1"},
    "has_tasks" true,
    "author"
    {"id" 0,
     "username" "smith.smith",
     "name" "Smith Smith",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/0/avatar.png",
     "web_url" "https://gitlab.com/smith.smith"},
    "service_desk_reply_to" nil,
    "milestone" nil,
    "epic"
    {"id" 1001,
     "iid" 1,
     "title" "My epic 1",
     "url" "/groups/my-group/-/epics/1",
     "group_id" 0},
    "id" 1001,
    "upvotes" 0,
    "discussion_locked" nil,
    "assignees"
    [{"id" 1,
      "username" "john.doe",
      "name" "John Doe",
      "state" "active",
      "locked" false,
      "avatar_url"
      "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
      "web_url" "https://gitlab.com/john.doe"}],
    "labels" ["label1" "key1::value1" "key2::value2" "label2"],
    "confidential" false,
    "updated_at" "2024-02-26T13:58:03.684Z",
    "iteration" nil,
    "blocking_issues_count" 0,
    "title" "Update vulnerable package",
    "assignee"
    {"id" 1,
     "username" "john.doe",
     "name" "John Doe",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
     "web_url" "https://gitlab.com/john.doe"},
    "type" "ISSUE",
    "task_completion_status" {"count" 0, "completed_count" 0},
    "issue_type" "issue",
    "project_id" 1,
    "state" "opened",
    "iid" 1,
    "time_stats"
    {"time_estimate" 0,
     "total_time_spent" 0,
     "human_time_estimate" nil,
     "human_total_time_spent" nil},
    "task_status" "",
    "created_at" "2024-02-23T14:35:50.584Z",
    "moved_to_id" nil,
    "user_notes_count" 0,
    "severity" "UNKNOWN",
    "downvotes" 0,
    "closed_at" "2024-02-26T13:58:03.671Z",
    "weight" 1,
    "description"
    "We need to upgrade package X to version Y because of CVE Z",
    "epic_iid" 1001,
    "merge_requests_count" 0}
   1002
   {"web_url" "https://gitlab.com/my-group/project1/-/issues/2",
    "due_date" nil,
    "_links"
    {"self" "https://gitlab.com/api/v4/projects/1/issues/2",
     "notes" "https://gitlab.com/api/v4/projects/1/issues/2/notes",
     "award_emoji"
     "https://gitlab.com/api/v4/projects/1/issues/2/award_emoji",
     "project" "https://gitlab.com/api/v4/projects/1",
     "closed_as_duplicate_of" nil},
    "references"
    {"short" "#2", "relative" "#2", "full" "my-group/project1#2"},
    "has_tasks" true,
    "author"
    {"id" 0,
     "username" "john.doe",
     "name" "John Doe",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/0/avatar.png",
     "web_url" "https://gitlab.com/john.doe"},
    "service_desk_reply_to" nil,
    "milestone" nil,
    "epic"
    {"id" 1002,
     "iid" 2,
     "title" "Other epic 2",
     "url" "/groups/my-group/-/epics/2",
     "group_id" 0},
    "id" 1002,
    "upvotes" 0,
    "discussion_locked" nil,
    "assignees"
    [{"id" 1,
      "username" "smith.smith",
      "name" "Smith Smith",
      "state" "active",
      "locked" false,
      "avatar_url"
      "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
      "web_url" "https://gitlab.com/smith.smith"}
     {"id" 0,
      "username" "john.doe",
      "name" "John Doe",
      "state" "active",
      "locked" false,
      "avatar_url"
      "https://gitlab.com/uploads/-/system/user/avatar/0/avatar.png",
      "web_url" "https://gitlab.com/john.doe"}],
    "labels" ["label1" "key1::value1" "key2::value2" "label2"],
    "confidential" false,
    "updated_at" "2024-02-26T13:58:03.684Z",
    "iteration" nil,
    "blocking_issues_count" 0,
    "title" "Use new api",
    "assignee"
    {"id" 1,
     "username" "smith.smith",
     "name" "Smith Smith",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
     "web_url" "https://gitlab.com/smith.smith"},
    "type" "ISSUE",
    "task_completion_status" {"count" 0, "completed_count" 0},
    "issue_type" "issue",
    "project_id" 1,
    "state" "opened",
    "iid" 2,
    "time_stats"
    {"time_estimate" 0,
     "total_time_spent" 0,
     "human_time_estimate" nil,
     "human_total_time_spent" nil},
    "task_status" "",
    "created_at" "2024-02-23T14:35:50.584Z",
    "moved_to_id" nil,
    "user_notes_count" 0,
    "severity" "UNKNOWN",
    "downvotes" 0,
    "closed_at" "2024-02-26T13:58:03.671Z",
    "weight" 1,
    "description" "Once the new api of service 2 done, use it",
    "epic_iid" 1002,
    "merge_requests_count" 0}
   2001
   {"web_url" "https://gitlab.com/my-group/project2/-/issues/1",
    "due_date" nil,
    "_links"
    {"self" "https://gitlab.com/api/v4/projects/2/issues/1",
     "notes" "https://gitlab.com/api/v4/projects/2/issues/1/notes",
     "award_emoji"
     "https://gitlab.com/api/v4/projects/2/issues/1/award_emoji",
     "project" "https://gitlab.com/api/v4/projects/2",
     "closed_as_duplicate_of" nil},
    "references"
    {"short" "#1", "relative" "#1", "full" "my-group/project2#1"},
    "has_tasks" true,
    "author"
    {"id" 0,
     "username" "smith.smith",
     "name" "Smith Smith",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/0/avatar.png",
     "web_url" "https://gitlab.com/smith.smith"},
    "service_desk_reply_to" nil,
    "milestone" nil,
    "epic"
    {"id" 1001,
     "iid" 1,
     "title" "My epic 1",
     "url" "/groups/my-group/-/epics/1",
     "group_id" 0},
    "id" 2001,
    "upvotes" 0,
    "discussion_locked" nil,
    "assignees"
    [{"id" 1,
      "username" "john.doe",
      "name" "John Doe",
      "state" "active",
      "locked" false,
      "avatar_url"
      "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
      "web_url" "https://gitlab.com/john.doe"}],
    "labels" ["label1" "key1::value1" "key2::value2" "label2"],
    "confidential" false,
    "updated_at" "2024-02-26T13:58:03.684Z",
    "iteration" nil,
    "blocking_issues_count" 0,
    "title" "Create new service interface",
    "assignee"
    {"id" 1,
     "username" "john.doe",
     "name" "John Doe",
     "state" "active",
     "locked" false,
     "avatar_url"
     "https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
     "web_url" "https://gitlab.com/john.doe"},
    "type" "ISSUE",
    "task_completion_status" {"count" 0, "completed_count" 0},
    "issue_type" "issue",
    "project_id" 2,
    "state" "opened",
    "iid" 1,
    "time_stats"
    {"time_estimate" 0,
     "total_time_spent" 0,
     "human_time_estimate" nil,
     "human_total_time_spent" nil},
    "task_status" "",
    "created_at" "2024-02-23T14:35:50.584Z",
    "moved_to_id" nil,
    "user_notes_count" 0,
    "severity" "UNKNOWN",
    "downvotes" 0,
    "closed_at" "2024-02-26T13:58:03.671Z",
    "weight" 1,
    "description" "No desc, sorry :(",
    "epic_iid" 1001,
    "merge_requests_count" 0}})

(comment
  (map
   (fn [v]
     {(get v "id") (get v "title")})
   (vals issue-by-general-id)))

(def links-by-issue-id
  {1001
   [(assoc (get issue-by-general-id 1002)
           "issue_link_id" 1,
           "link_type" "blocks",
           "link_created_at" "2024-01-03T10:24:56.954Z",
           "link_updated_at" "2024-01-03T10:24:56.954Z")]
   1002
   [(assoc (get issue-by-general-id 1001)
           "issue_link_id" 1,
           "link_type" "is_blocked_by",
           "link_created_at" "2024-01-03T10:24:56.954Z",
           "link_updated_at" "2024-01-03T10:24:56.954Z")
    (assoc (get issue-by-general-id 2001)
           "issue_link_id" 2,
           "link_type" "is_blocked_by",
           "link_created_at" "2024-01-03T10:24:56.954Z",
           "link_updated_at" "2024-01-03T10:24:56.954Z")]
   2001
   [(assoc (get issue-by-general-id 1002)
           "issue_link_id" 2,
           "link_type" "blocks",
           "link_created_at" "2024-01-03T10:24:56.954Z",
           "link_updated_at" "2024-01-03T10:24:56.954Z")]})

(def core-links-by-issue-id
  (reduce
   (fn [acc [k v]]
     (assoc acc k (map link/api-map->link v)))
   {}
   links-by-issue-id))
