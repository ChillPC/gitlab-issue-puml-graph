(ns gitlab.issue-test
  (:require [clojure.test :refer [are deftest testing is]]
            [gitlab.issue-fixture :as fixt]
            [core.gitlab.issue :as issue]))

(deftest raw-labels->labels-props
  (testing "Only labels"
    (is (= (issue/-raw-labels->labels-props
            ["API" "Frontend"])
           {nil #{"API" "Frontend"}})))

  (testing "Only props"
    (is (= (issue/-raw-labels->labels-props
            ["workflow::In progress" "status::Deployed"])
           {"workflow" "In progress"
            "status" "Deployed"})))

  (testing "2 labels of each kind"
    (is (= (issue/-raw-labels->labels-props
            ["workflow::In progress" "API" "Frontend" "status::Deployed"])
           {nil #{"API" "Frontend"}
            "workflow" "In progress"
            "status" "Deployed"}))))

(comment
  (issue/api-map->issue (-> fixt/map-fixture (rest) (first))))
