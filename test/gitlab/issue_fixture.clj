(ns gitlab.issue-fixture
  (:require [clj-json.core :as json]))

(def str-fixture
  (slurp "test/gitlab/issue_fixture.json"))

(def map-fixture
  (json/parse-string str-fixture))
