# Gitlab issue dep graph

This project aims to visualize linked gitlab issues in a graph using plantuml.

Roadmap :

- [X] Modelize a gitlab issue into a plantuml
- [X] Labels with `::` as map key value
- [X] Generate multiple issues in plantuml
- [ ] != Pin for != status (to determine)
- [X] Fetch gitlab issues to make plantuml
- [ ] Highlight issue (color, bold...) with a filter (assignee to me...)
- Epic :
  - [ ] Grow epic collection over gitlab issues
  - [ ] 
- Links :
  - [ ] Sort link issues into maps into original issues
  - [ ] Draw arrows between issues
  - [ ] Avoid duplicate of issues
  - [ ] Fetch links for each issue
- Filtering by :
  - [ ] Project
  - [ ] Group
  - [ ] Epic
  - [ ] Opened/Closed
  - [ ] Labels
  - [ ] When filtering, option to allow just the nearest linked issue
- [ ] Apply :
  - https://github.com/taylorwood/clj.native-cli
  - https://nikvdp.com/post/bbb/
  - https://www.astrecipes.net/blog/2018/07/20/cmd-line-apps-with-clojure-and-graalvm/
